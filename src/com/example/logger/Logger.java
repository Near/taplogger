package com.example.logger;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.widget.TextView;

public class Logger extends Activity {
	private LoggerView loggerView;
	private TextView inference;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_logger);
		inference = (TextView)findViewById(R.id.inference);
        loggerView = (LoggerView)findViewById(R.id.loggerView);
        loggerView.setModel(inference);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.logger, menu);
		return true;
	}

}
