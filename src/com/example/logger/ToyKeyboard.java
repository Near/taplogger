package com.example.logger;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.Button;
import android.widget.LinearLayout;

public class ToyKeyboard extends LinearLayout {
	Button[] bts = new Button[12];
	LinearLayout[] l = new LinearLayout[4];
	@SuppressWarnings("deprecation")
	public ToyKeyboard(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context, attrs);
		this.setOrientation(VERTICAL);
		this.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT));
		for(int i = 0; i < 4; i ++) {
			l[i] = new LinearLayout(context, attrs);
			l[i].setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT, 1));
			for(int j = 0; j < 3; j ++) l[i].addView(bts[i*3+j], j);
			this.addView(l[i]);
		}
	}
	
	
	@SuppressWarnings("deprecation")
	private void init(Context context, AttributeSet attrs) {
		for(int i = 0; i < 12; i ++) {
			bts[i] = new Button(context, attrs) {
				public boolean onTouchEvent(MotionEvent event) {
					return true;
				}
			};
			bts[i].setId(i);
			bts[i].setText("" + i);
			bts[i].setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT, 1));
		}
	}
}

