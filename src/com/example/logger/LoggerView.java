package com.example.logger;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;

import libsvm.svm;
import libsvm.svm_model;
import libsvm.svm_node;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Environment;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.LinearLayout;
import android.widget.TextView;

public class LoggerView extends LinearLayout implements SensorEventListener {
	private static final double base = 9.8*9.8, tolerance = 2, mis = 0.1;
	private static final int numofGuess = 4;
	private final SensorManager mSensorManager;
	private final Sensor mAccelerometer, mOrientation;
	private Queue<Double> sqsumRecords, rollRecords, pitchRecords;
	private double feature[][];
	private int checkpoint, monitorWin, count = 0;
	private svm_model model;
	private TextView inference;

	@SuppressWarnings("deprecation")
	public LoggerView(Context context, AttributeSet attr) {
		super(context, attr);
		mSensorManager = (SensorManager)context.getSystemService(Context.SENSOR_SERVICE);
		mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
		mOrientation = mSensorManager.getDefaultSensor(Sensor.TYPE_ORIENTATION);
		init(Environment.getExternalStorageDirectory().getPath() + "/tapdata.txt");
		try {
			model = svm.svm_load_model(Environment.getExternalStorageDirectory().getPath() + "/output");
		} catch (Exception e) {
			e.printStackTrace();
		}
		ToyKeyboard keyboard = new ToyKeyboard(context, attr);
		addView(keyboard);
		keyboard.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT, 1));
		Log.v("size", "" + rollRecords.size() + " " + monitorWin);
		mSensorManager.registerListener(this, mAccelerometer, SensorManager.SENSOR_DELAY_FASTEST);
		mSensorManager.registerListener(this, mOrientation, SensorManager.SENSOR_DELAY_FASTEST);
	}

	@SuppressWarnings("resource")
	public void init(String path) {
		feature = new double[5][2];
		try {
			File f = new File(path);
			Log.v("file", "" + f.exists());
			BufferedReader br = new BufferedReader(new FileReader(f));
			for(int i = 0; i < 5; i ++) {
				for(int j = 0; j < 2; j ++) {
					feature[i][j] = Double.parseDouble(br.readLine());
					Log.v("" + i + j, ""+feature[i][j]);
				}
			}
			double sigWin = Double.parseDouble(br.readLine());
			Log.i("size", "" + sigWin);
			monitorWin = (int) sigWin * 2;
			checkpoint = (int) sigWin;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (NumberFormatException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		sqsumRecords = new LinkedList<Double>();
		rollRecords = new LinkedList<Double>();
		pitchRecords = new LinkedList<Double>();
		for (int i = 0; i < monitorWin; i++) {
			sqsumRecords.add(LoggerView.base);
			rollRecords.add(0.0);
			pitchRecords.add(0.0);
		}
	}

	public void setModel(TextView m) {
		inference = m;
	}

	private int[] getStatus() {
		Double[] mw = sqsumRecords.toArray(new Double[0]);
		for(int i = -checkpoint/2; i <= checkpoint/2 ; i ++)
			if(mw[checkpoint+i] > mw[checkpoint]+mis) {Log.i("what", ""); return new int[] {-1, -1};}
		if (!inRegion(mw[checkpoint] - base, feature[0])) {
			Log.i("f1", "" + (mw[checkpoint] - base) + " " + feature[0][0] + " " + feature[0][1]);
			return new int[] { -1, -1 };
		}
		int startIndex = checkpoint - 1, endIndex = checkpoint + 1;
		int troughIndex = checkpoint;
		for (; startIndex >= 1; startIndex--) {
			troughIndex = mw[troughIndex] > mw[startIndex] ? startIndex : troughIndex;
		}
		for(startIndex = troughIndex-1; startIndex >= 1; startIndex --) {
			if(Math.abs(mw[startIndex]-base) < tolerance ) break;
		}
		if (!inRegion(mw[troughIndex] - base, feature[1])) {
			Log.i("f2", "" + (mw[troughIndex] - base) + " " + feature[1][0] + " " + feature[1][1]);
			return new int[] { -1, -1 };
		}
		if (!inRegion(mw[checkpoint] - mw[troughIndex], feature[2])) {
			Log.i("f3", "" + (mw[checkpoint] - mw[troughIndex]) + " " + feature[2][0] + " " + feature[2][1]);
			return new int[] { -1, -1 };
		} 
		if (!inRegion(checkpoint - troughIndex, feature[3])) {
			Log.i("f4", "" + (checkpoint - troughIndex) + " " + feature[3][0] + " " + feature[3][1]);
			return new int[] { -1, -1 };
		}
		endIndex = startIndex + (int) monitorWin / 2;
		if (!inRegion(standDeviation(mw, startIndex, endIndex), feature[4])) {
			Log.i("f5", "" + standDeviation(mw, startIndex, endIndex) + " " + feature[4][0] + " " + feature[4][1]);
			return new int[] { -1, -1 };
		}
		for(int i = startIndex; i < endIndex; i ++)
			Log.v("" + i, "" + mw[i]);
		return new int[] { startIndex, endIndex };
	}

	public void getInference() {
		int[] f = getStatus();
		if (f[0] == -1) return;
		double f1[] = getThreeAttr(rollRecords, f[0], f[1]-1);
		double f2[] = getThreeAttr(pitchRecords, f[0], f[1]-1);
		double prob[] = new double[model.nr_class], temp[] = new double[model.nr_class];
		svm_node x[] = new svm_node[6];
		Log.i("feature1", "" + f1[0] + "," + f1[1] + "," + f1[2]);
		Log.i("feature2", "" + f2[0] + "," + f2[1] + "," + f2[2]);
		for (int i = 0; i < 3; i++) {
			x[i] = new svm_node();
			x[i].index = i + 1;
			x[i].value = f1[i];
			x[i+3] = new svm_node();
			x[i+3].index = i + 4;
			x[i+3].value = f2[i];
		}
		svm.svm_predict_probability(model, x, prob);
		temp = prob.clone();
		Arrays.sort(prob);
		String inf = "";
		for(int i = 0, c = 0; c < numofGuess; i ++) {
			for(int j = 0; j < model.nr_class && c < numofGuess; j ++)
				if(Math.abs(temp[j] - prob[model.nr_class - i - 1]) < 0.000001) {
					inf += "" + model.label[j] + " ";
					c ++;
				}
		}
		count += 1;
		inference.setText("" + count + " " + inf);
	}

	public void onAccuracyChanged(Sensor sensor, int accuracy) {
	}

	@SuppressWarnings("deprecation")
	public void onSensorChanged(SensorEvent event) {
		switch (event.sensor.getType()) {
		case Sensor.TYPE_ACCELEROMETER:
			double temp = LoggerView.getSqSum(event.values);
			sqsumRecords.remove();
			sqsumRecords.add(temp);
			getInference();
			break;
		case Sensor.TYPE_ORIENTATION:
			rollRecords.remove();
			pitchRecords.remove();
			rollRecords.add((double) event.values[2]);
			pitchRecords.add((double) event.values[1]);
			break;
		default:
			break;
		}
	}

	/*************** helper function *******************/
	private boolean inRegion(double a, double[] r) {
		return a >= r[0] && a <= r[1];
	}

	private static double getSqSum(float accele[]) {
		double sum = 0;
		for (int i = 0; i < 3; i++)
			sum += accele[i] * accele[i];
		return sum;
	}

	// [l, r)
	private double standDeviation(Object[] q, int l, int r) {
		double avg = 0, len = r - l, sum = 0;
		if(len == 1) return 0;
		for (int i = l; i < r; i++) avg += (Double) q[i] / len;
		for (int i = l; i < r; i++) sum += ((Double) q[i] - avg) * ((Double) q[i] - avg);
		return Math.sqrt(sum / (len - 1));
	}
	
	/*
	// [start, end]
	private double[] getThreeAttr(Queue<Double> q, int start, int end) {
		if(q.size() <= 1) return new double[]{0, 0, 0};
		Double[] array = q.toArray(new Double[0]);
		double f[] = new double[]{0, 0, array[end] - array[start]};
		int i = start+1;
		for(; i <= end && array[i] == array[start]; i ++);
		if(i > end) return f;
		boolean increase = (array[i] - array[start] > 0);
		for(; i <= end && mono(array[i-1], array[i], increase); i ++);
		f[0] = array[i-1] - array[start];
		if(i <= end) {
			int j = i;
			for(; j <= end && array[j] == array[i]; j ++);
			if(j > end) return f;
			for(; j <= end && mono(array[j], array[j-1], increase); j ++);
			f[1] = array[j-1] - array[i-1];
		}
		return f;
	}
	
	// non-strict mono
	private boolean mono(double p, double n, boolean increase) {
		if(p == n) return true;
		else return (n - p > 0) == increase;
	}
	*/
	
	private double[] getThreeAttr(Queue<Double> q, int start, int end) {
		if(q.size() <= 1) return new double[]{0, 0, 0};
		Double[] array = q.toArray(new Double[0]);
		double change = 0, max_inc = 0, max_dec = 0, step = 0;
		int inc = 2*end, dec = 2*end;
		for(int i = start+1; i <= end+1; i ++) {
			if(i <= end) step = array[i] - array[i-1];
			else step = -change;
			if(step * change >= 0) change += step;
			else {
				if(change > 0 && max_inc <= change+mis) {
					inc = i;
					max_inc = change;
				} else if(change < 0 && max_dec >= change-mis) {
					dec = i;
					max_dec = change;
				}
				change = step;
			}
		}
		String s = "";
		for(int i = start; i <= end; i ++) s += "" + array[i] + ",";
		Log.i("feature", s);
		if(inc > dec) return new double[] {max_dec, max_inc, array[end] - array[start]};
		else return new double[] {max_inc, max_dec, array[end] - array[start]};
	}
}